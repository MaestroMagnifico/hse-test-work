<?php
/**
 * Этот файл инициализирует всё что нужно. Может использоваться в index.php, AJAX, CRON
 */

// debug
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
setlocale(LC_ALL, 'ru_RU.utf8');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// абсолютный путь к сайту (для CRON)
define('SITE_DIR', str_replace('\\', '/', __DIR__).'/');
// конфиг
require_once(SITE_DIR.'config.php');

// подключаем необходимые компоненты
require_once(SITE_DIR.'includes/db.php');
require_once(SITE_DIR.'includes/modify.php');
require_once(SITE_DIR.'includes/templates.php');
require_once(SITE_DIR.'includes/dates.php');
require_once(SITE_DIR.'includes/email.php');
require_once(SITE_DIR.'vendor/autoload.php');

// подключаемся к базе данных
$db = new db();
$db->connect_db();

// проверяем авторизацию
require_once(SITE_DIR.'modules/auth.php');
$auth = new auth();
$user = $auth->check_session();
define('USER', $user);
unset($user);

// модульная система
$module = $_REQUEST['module'];
if ((!$module) and (!$_POST['ajaxing'] and $_POST['ajaxing'] != 'undefined')) {
	// модуль по-умолчанию
	$module = 'contact';
}
if (!in_array($module, REGISTERED_MODULES)) {
	// 404
	$module = false;
}
define('MODULE', $module);
unset($module);