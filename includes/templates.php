<?php

/**
 * Типовые шаблоны, используемые на всех страницах
 * Class templates
 */
class templates {
	/**
	 * Вывод сообщения об ошибке
	 * @param string $title
	 * @param string $message
	 * @param bool $die
	 */
	public function error(string $title, string $message, bool $die = true) {
		if ($title) {
			$title = '<b>'.$title.'</b><br>';
		}
		echo '<div class="alert alert-danger" role="alert">'.$title.''.$message.'</div>';
		if ($die) {
			require_once(SITE_DIR.'templates/footer.php');
			die;
		}
	}
	
	/**
	 * Вывод ошибки для AJAX
	 * @param string $message - сообщение
	 * @param int $error - тип ошибки (для JS)
	 * @param array $hl - названия полей в форме, которые нужно подсветить
	 */
	public function json_error(string $message, int $error = 1, array $hl = array()) {
		$arr = array('message' => $message, 'error' => $error);
		if (count($hl)) {
			$arr['fields'] = $hl;
		}
		echo json_encode($arr);
		die;
	}
	
	/**
	 * Проверка обязательных полей для AJAX
	 * @param array $data - полученные данные, поле => значение
	 * @param array $req - массив обязательных полей
	 */
	public function check_req(array $data, array $req) {
		$empty = array();
		foreach ($req as $item) {
			if (!$data[$item]) {
				$empty[] = $item;
			}
		}
		if (count($empty)) {
			$this->json_error('Не все необходимые поля были заполнены', 1, $empty);
		}
	}
	
	/**
	 * Редирект через header использовать нельзя - headers already sent
	 * Поэтому используем редирект через JS
	 * @param string $url
	 */
	public function redirect(string $url) {
		echo '
		<script type="text/javascript">
			window.location.href = "'.$url.'";
		</script>
		';
		require_once(SITE_DIR.'templates/footer.php');
		die;
	}
}