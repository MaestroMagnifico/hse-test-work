<?php

/**
 * Класс для преобразования строк и чисел в нужный вид
 * Class modify
 */
class modify {
	/**
	 * Формируем из текста какой-то пригодный формат
	 * @param $text - входной текст
	 * @param string $mode - формат
	 * @return string - выходной текст
	 */
	public function form_text(string $text, string $mode = 'output'):string {
		if (is_array($text)) {
			return $text;
		}
		
		// формирует текст под разные ситуации
		switch ($mode) {
			case 'output':
				// текст из бд -> вывод на страницу
				$text = stripslashes($text);
				$text = htmlspecialchars($text);
				$text = nl2br($text, false);
			break;
			case 'input':
				// текст из бд -> поле ввода
				$text = stripslashes($text);
				$text = htmlentities($text);
			break;
		}
		return $text;
	}
}