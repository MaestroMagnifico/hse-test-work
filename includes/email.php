<?php
use PHPMailer\PHPMailer\PHPMailer;

class email {
	/**
	 * @param string $to
	 * @param string $msg
	 * @param string $subject
	 * @param string $filepath
	 * @return bool
	 * @throws \PHPMailer\PHPMailer\Exception
	 */
	public function send_email(string $to, string $msg, string $subject, string $filepath = ''):bool {
		//SMTP needs accurate times, and the PHP time zone MUST be set
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		$mail->CharSet = 'utf-8';
		//Tell PHPMailer to use SMTP
		$mail->isSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;
		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';
		//Set the hostname of the mail server
		$mail->Host = EMAIL_HOST;
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->Port = EMAIL_PORT;
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = EMAIL_USER;
		//Password to use for SMTP authentication
		$mail->Password = EMAIL_PASSWORD;
		//Set who the message is to be sent from
		$mail->setFrom(EMAIL_USER, SITE_NAME);
		//Set an alternative reply-to address
		$mail->addReplyTo(EMAIL_USER, SITE_NAME);
		//Set who the message is to be sent to
		$mail->addAddress($to);
		//Set the subject line
		$mail->Subject = $subject;
		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		$mail->msgHTML($msg);
		//Replace the plain text body with one created manually
		//$mail->AltBody = strip_tags($html);
		//Attach an image file
		if (is_array($filepath)) {
			foreach ($filepath as $v) {
				$mail->addAttachment($v);
			}
		} else {
			if ($filepath) {
				$mail->addAttachment($filepath);
			}
		}
		
		//send the message, check for errors
		if ($mail->send()) {
			return true;
		} else {
			return false;
		}
	}
}