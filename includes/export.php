<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
require '../init.php';
if (!USER) {
	die('У вас нет доступа к этой функции.');
}

$db = new db();
$modify = new modify();
$dates = new dates();
$tpl = new templates();

switch($_GET['mode']) {
	case 'xls':
		$spreadsheet = new Spreadsheet();
		
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'ФИО');
		$sheet->setCellValue('B1', 'Телефон');
		$sheet->setCellValue('C1', 'E-mail');
		$sheet->setCellValue('D1', 'Время звонка');
		
		$n = 1;
		$query = $db->query("SELECT * FROM `contact`");
		$db->check_mysqli_error();
		while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
			$n++;
			$sheet->setCellValue('A'.$n, $modify->form_text($row['fio']));
			$sheet->setCellValue('B'.$n, $modify->form_text($row['phone']));
			$sheet->setCellValue('C'.$n, $modify->form_text($row['email']));
			$sheet->setCellValue('D'.$n, $dates->out_date($row['contact_date'], 'full_dig'));
		}
		
		$writer = new PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="export.xls"');
		$writer->save("php://output");
		break;
	case 'csv':
		// здесь я не использовал Spreadsheet, потому что там есть проблема с кодировкой
		$data = "ФИО;Телефон;E-mail;Время звонка;\r\n";
		
		$query = $db->query("SELECT * FROM `contact`");
		$db->check_mysqli_error();
		while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
			$data .= $modify->form_text($row['fio']).';';
			$data .= $modify->form_text($row['phone']).';';
			$data .= $modify->form_text($row['email']).';';
			$data .= $dates->out_date($row['contact_date'], 'full_dig').';';
			$data .= "\r\n";
		}
		
		$data = mb_convert_encoding($data, 'CP1251', 'UTF-8');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="export.csv"');
		echo $data;
		
		break;
	default:
		$tpl->error('Ошибка', 'Неразрешённый формат');
		break;
}




