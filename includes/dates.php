<?php

/**
 * Работа с датами
 * Class dates
 */
class dates {
	/**
	 * Конвертор из локального времени в Гринвич
	 * @param string $date - Дата в формате Y-m-d H:i:s
	 * @param string $format - Выходной формат
	 * @return string
	 * @throws Exception
	 */
	public function local_to_utc(string $date, string $format = 'Y-m-d H:i:s'):string {
		// предохранитель от кривых рук
		if (!substr_count($date, ' ')) {
			$date .= ' 12:00:00';
		}
		
		$date = new DateTime($date, new DateTimeZone($this->get_user_timezone(true)));
		$time_zone = new DateTimeZone('UTC');
		
		$date->setTimezone($time_zone);
		return $date->format($format);
	}
	
	/**
	 * Конвертор из Гринвича в локальное время. Берётся часовой пояс сотрудника.
	 * @param string $date - Дата в формате Y-m-d H:i:s
	 * @param string $format - Выходной формат
	 * @return string
	 */
	public function utc_to_local(string $date, string $format = 'Y-m-d H:i:s'):string {
		// предохранитель от кривых рук
		if (!substr_count($date, ' ')) {
			$date .= ' 12:00:00';
		}
		
		try {
			$date = new DateTime($date, new DateTimeZone('UTC'));
		} catch (Exception $e) {
			die('Фатальная ошибка при конвертации даты');
		}
		$time_zone = new DateTimeZone($this->get_user_timezone(true));
		
		$date->setTimezone($time_zone);
		
		return $date->format($format);
	}
	
	/**
	 * Вытаскиваем часовой пояс сотрудника
	 * @param bool $return_string - Нужно ли вернуть строку в формате Europe/Moscow или количество часов относительно Гринвича
	 * @return mixed
	 */
	public function get_user_timezone(bool $return_string=false) {
		if ($return_string) {
			return 'Europe/Moscow';
		} else {
			return 3;
		}
	}
	
	/**
	 * Вывод дат в читабельный формат
	 * @param string $time_in - Дата в форматах Y-m-d H:i:s или Y-m-d
	 * @param string $format - Выходной формат
	 * @param bool $gmt_offset - Нужно ли перевести из Гринвича в локальное время
	 * @return string
	 */
	public function out_date(string $time_in, string $format = 'full', bool $gmt_offset = false):string {
		if (!$time_in) {
			return '';
		}
		if (substr($time_in, 0, 10) == '0000-00-00') {
			return '';
		}
		
		if ($format == 'full_dig' and !substr_count($time_in, ' ')) {
			$format = 'tiny_dig';
		}
		
		if ($gmt_offset) {
			$time_in = $this->utc_to_local($time_in);
		}
		
		list($date, $time) = explode(' ', $time_in);
		list($year, $month, $day) = explode('-', $date);
		if ($time) {
			list($hour, $minute, $sec) = explode(':', $time);
		} else {
			$hour = $minute = $sec = '00';
		}
		
		$locale = array(
			1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря',
		);
		
		switch ($format) {
			case 'full':
				$month = $locale[(int)$month];
				$time_in = $day.' '.$month.' '.$year.' г., '.$hour.':'.$minute;
			break;
			case 'compact':
				$month = $locale[(int)$month];
				$time_in = $day.' '.$month.' '.$year.' г.';
			break;
			case 'full_dig':
				$time_in = $day.'.'.$month.'.'.$year.', '.$hour.':'.$minute;
			break;
			case 'compact_dig':
				$time_in = $day.'.'.$month.'.'.$year;
			break;
		}
		
		return $time_in;
	}
	
	/**
	 * Проверка даты на валидность
	 * @param string $date
	 * @return bool
	 */
	public function is_valid_date(string $date):bool {
		$date = trim($date);
		list($date, $time) = explode(' ', $date);
		list($year, $month, $day) = explode('-', $date);
		if (!checkdate($month, $day, $year)) {
			return false;
		}
		
		if ($time) {
			list($hours, $minutes, $seconds) = explode(':', $time);
			if (!(int)$hours and !(int)$minutes and !(int)$seconds) {
				return false;
			}
			if ((int)$hours < 0 or (int)$hours > 23) {
				return false;
			}
			if ((int)$minutes < 0 or (int)$minutes > 59) {
				return false;
			}
			if ((int)$seconds < 0 or (int)$seconds > 59) {
				return false;
			}
		}
		return true;
	}
	
}