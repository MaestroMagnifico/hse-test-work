<?php

/**
 * Работа с базой данных
 * Class db
 */
class db {
	private static $db_link;
	
	/**
	 * Подключаемся к базе данных
	 * @return mysqli - db link
	 */
	public function connect_db() {
		// не подключаемся дважды
		if ($this::$db_link) {
			return $this::$db_link;
		}
		
		$tpl = new templates();
		
		$db_link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		if (mysqli_connect_errno()) {
			$tpl->error('Не удалось подключиться к базе данных', mysqli_connect_error());
		}
		if (!mysqli_set_charset($db_link, "utf8")) {
			$tpl->error('Ошибка базы данных', mysqli_error($db_link));
		}
		$this::$db_link = $db_link;
		return $this::$db_link;
	}
	
	/**
	 * Обычная mysqli_query с упращённым синтаксисом
	 * @param string $query - SQL-запрос
	 * @return bool|mysqli_result
	 */
	public function query(string $query) {
		return mysqli_query($this::$db_link, $query);
	}
	
	/**
	 * Обычное экрнирование с упращённым синтаксисом
	 * @param $text - строка
	 * @return string
	 */
	public function escape($text) {
		return mysqli_real_escape_string($this::$db_link, $text);
	}
	
	/**
	 * Проверка ошибки после выполения SQL запроса с выводом ошибки
	 * @param string $add - дополнительный текст ошибки
	 * @return bool
	 */
	public function check_mysqli_error(string $add = '') {
		$error = mysqli_error($this::$db_link);
		if (!$error) {
			return false;
		}
		
		if ($add) {
			$error = '<b>'.$add.': </b><br>'.$error;
		}
		if ($_POST['ajaxing'] and $_POST['ajaxing'] != 'undefined') {
			echo json_encode(array('message' => $error, 'error' => 1));
			die;
		} else {
			$tpl = new templates();
			$tpl->error($error, mysqli_error($this::$db_link));
		}
		return true;
	}
	
	/**
	 * Экранирует строку для обращения в БД
	 * @param string $string
	 * @return mixed
	 */
	public function secure_str(string $string) {
		$string = trim($string);
		$string = addslashes($string);
		$string = $this->escape($string);
		return $string;
	}
	
	/**
	 * Экранирует массив строк для обращения в БД
	 * @param array $arr
	 * @return mixed
	 */
	public function secure_array(array $arr) {
		foreach ($arr as $key => $value) {
			$arr[$key] = $this->secure_str($value);
		}
		return $arr;
	}
}