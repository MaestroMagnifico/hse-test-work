# Высшая школа экономики: тестовая работа

Автор: https://maestro-magnifico.ru/

### Установка

- Качаем [Composer](https://getcomposer.org/composer-stable.phar) в папку проекта;
- Открываем `cmd`, переходим в папку проекта командой `cd`;
- Качаем зависимости: `php composer.phar install`;
- Импортируем базу, дамп лежит в корне проекта: `hse.sql`;
- Переименовываем `config.php.sample` в `config.php` и заполняем его;
- В базе уже создан администратор. Логин: `admin@admin.ru`, пароль: `123`.


### Используемые библиотеки

- PHPMailer. Для отправки почты через SMTP. Эта библиотека встроена в Wordpress по-умолчанию;
- PhpSpreadsheet. Для создания файлов Excel;
- Bootstrap. Для упрощения вёрстки;
- jQuery. Для AJAX и всего остального;
- Maskedinput. Маска для телефона. Этот плагин хоть и старый, но он работает лучше, чем новый jQuery Mask.

### Особенности

- Базовая модульная система;
- Файл `init.php` для проверки и инициализации всего необходимого;
- Универсальный ajax-обработчик `ajax.php`;
- HTML-шаблоны вынесены в отдельные файлы в папке `templates`;
- В папке `includes` лежит необходимый функционал, не относящийся к модулям.