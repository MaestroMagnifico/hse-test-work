<div class="container">
	<div class="d-flex justify-content-between mb-3">
		<a href="#" class="jsSignOut">&larr; Выход</a>
	</div>
	
	<h1>Добро пожаловать, <?php echo $data['name']; ?>!</h1>
	<table class="table">
		<tbody>
			<tr>
				<th scope="row" class="w-20">E-mail</th>
				<td><?php echo $data['email']; ?></td>
			</tr>
			<tr>
				<th scope="row">Телефон</th>
				<td><?php echo $data['phone']; ?></td>
			</tr>
			<tr>
				<th scope="row">Дата регистрации</th>
				<td><?php echo $data['date_register']; ?></td>
			</tr>
			<?php if ($data['photo']) { ?>
			<tr>
				<th scope="row">Фото</th>
				<td><a href="/uploads/<?php echo $data['photo']; ?>" target="_blank"><img src="/uploads/<?php echo $data['photo_thumb']; ?>" alt=""></a></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>