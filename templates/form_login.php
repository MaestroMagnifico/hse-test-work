<div class="form-container">
	<h1>Авторизация</h1>
	<form class="jsAuth">
		<label for="email">E-mail:</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="email" name="email" maxlength="255">
		</div>
		
		<label for="password">Пароль:</label>
		<div class="input-group mb-3">
			<input type="password" class="form-control" id="password" name="password">
		</div>
		
		<div class="text-center">
			<button type="submit" class="btn btn-success">Войти</button>
		</div>
	</form>
	<div class="mt-3 text-center"><a href="/">&larr; На главную страницу</a></div>
</div>