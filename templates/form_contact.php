<div class="form-container">
	<h1>Заявка на обратный звонок</h1>
	<form class="jsContact">
		<label for="name">ФИО:</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="fio" name="fio" maxlength="255">
		</div>
		
		<label for="phone">Телефон:</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="phone" name="phone" maxlength="255">
		</div>
		
		<label for="email">E-mail:</label>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="email" name="email" maxlength="255">
		</div>
		
		<div class="row mb-3">
			<div class="col">
				<label for="date">Дата звонка:</label>
				<input type="date" class="form-control" id="date" name="date" maxlength="255">
			</div>
			<div class="col">
				<label for="time">Время звонка:</label>
				<input type="time" class="form-control" id="time" name="time" maxlength="255">
			</div>
		</div>
		
		<div class="text-center">
			<button type="submit" class="btn btn-success">Отправить</button>
		</div>
		
		<input type="hidden" name="ajaxing" value="ajax_contact">
		<input type="hidden" name="module" value="contact">
	</form>
</div>