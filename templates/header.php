<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/styles.css?v=1">
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="/js/scripts.js?v=1"></script>
	
	<title><?php echo SITE_NAME; ?></title>
</head>
<body class="<?php echo MODULE; ?>">
	
	<header>
		<div class="container d-flex justify-content-between">
			<div class="logo-container">
				<a href="/" class="d-flex justify-content-between">
					<img class="mr-2" src="/css/images/logo.png" alt="">
					<div><?php echo SITE_NAME; ?></div>
				</a>
			</div>
			<nav>
				<ul class="d-flex justify-content-between">
					<li class="d-flex justify-content-end">
						<a href="/?module=auth"><?php echo USER ? 'Привет, <b>'.USER['name'].'</b>' : 'Вход' ?></a>
						<?php echo USER ? '<span class="jsSignOut ml-1">(выйти)</span>' : '' ?>
					</li>
				</ul>
			</nav>
		</div>
	</header>
	
	<main>
		<div class="container">