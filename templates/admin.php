<h1>Заявки на обратный звонок</h1>

<table class="table table-bordered">
	<thead>
	<tr>
		<th scope="col">#</th>
		<th scope="col">ФИО</th>
		<th scope="col">Телефон</th>
		<th scope="col">E-mail</th>
		<th scope="col">Время звонка</th>
	</tr>
	</thead>
	<tbody>
		<?php
		// из PHP такой себе шаблонизатор, но я тут стараюсь делать проще
		$n = 1;
		foreach ($rows as $data) {
		?>
		<tr>
			<th><?php echo $n; ?></th>
			<td><?php echo $data['fio']; ?></td>
			<td><?php echo $data['phone']; ?></td>
			<td><?php echo $data['email']; ?></td>
			<td><?php echo $data['contact_date']; ?></td>
		</tr>
		<?php
			$n++;
		}
		?>
	</tbody>
</table>

<div class="text-right">
	<a type="button" class="btn btn-info" href="/includes/export.php?mode=csv">Экспорт в CSV</a>
	<a type="button" class="btn btn-success" href="/includes/export.php?mode=xls">Экспорт в XLS</a>
</div>