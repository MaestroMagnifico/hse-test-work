/**
 * Подсветка полей с ошибками на странице
 * @param Fields
 */
function hl_inputs(Fields) {
	for (let i in Fields) {
		$('textarea[name="'+Fields[i]+'"]').addClass('field-error');
		let Obj = $('input[name="'+Fields[i]+'"]');
		Obj.addClass('field-error');
		
		// для чекбоксов, подсвечиваем их label
		Obj.each(function() {
			if ($(this).attr('type') == 'checkbox') {
				let Id = $(this).attr('id');
				$('label[for="'+Id+'"]').addClass('label-error');
			}
		});
	}
}

/**
 * Вывод ошибок
 * @param formObj - форма, в которую надо вывести ошибку
 * @param Message - сообщение об ошибке
 * @param Fields - массив полей, которые надо подсветить
 */
function error(formObj, Message, Fields) {
	if (!formObj.length) {
		// потом здесь можно будет сделать что-то покрасивее
		alert(Message);
	} else {
		let Container = formObj.find('.error-container');
		if (!Container.length) {
			formObj.prepend('<div class="alert alert-danger error-container" role="alert"></div>');
			Container = formObj.find('.error-container');
		}
		formObj.find('.success-container').remove();
		Container.html(Message).slideDown(300).addClass('active');
		
		if (Fields) {
			hl_inputs(Fields);
		}
	}
}

/**
 * Вывод успешного сообщения
 * @param formObj - форма, в которую надо вывести
 * @param Message - сообщение
 */
function success(formObj, Message) {
	if (!formObj.length) {
		// потом здесь можно будет сделать что-то покрасивее
		alert(Message);
	} else {
		let Container = formObj.find('.success-container');
		if (!Container.length) {
			formObj.prepend('<div class="alert alert-success success-container" role="alert"></div>');
			Container = formObj.find('.success-container');
		}
		formObj.trigger('reset').find('.field-error').removeClass('field-error');
		formObj.find('.error-container').remove();
		Container.html(Message).slideDown(300).addClass('active');
	}
}

/**
 * Простой AJAX с редиректом или сбросом формы
 * @param Data
 * @param formObj
 * @param fileUploadMode
 */
function ajax(Data, formObj, fileUploadMode) {
	// дефолтные значения
	let contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
	let processData = true;
	
	// нужно при загрузке файла
	if (fileUploadMode) {
		contentType = false;
		processData = false;
	}
	
	$.ajax({
		type: 'POST',
		cache: false,
		url: 'ajax.php',
		dataType: 'json',
		data: Data,
		contentType: contentType,
		processData: processData,
		success: function(result) {
			if (result['error']) {
				error(formObj, result['message'], result['fields']);
			} else {
				$('.error-container.active').slideUp(300);
				if (result['redirect']) {
					window.location.href = result['redirect'];
				} else {
					success(formObj, result['message']);
				}
			}
		},
		error: function(result){
			if ((result['status']) || (result['responseText'])) {
				error(formObj,'Error: '+result['status']+': '+result['responseText']);
			}
		}
	});
}

/**
 * Инициализация необходимого после загрузки контента
 */
function init_stuff() {
	$('input[name="phone"]').mask('+7 (999) 999-99-99');
}

$(document).ready(function() {
	init_stuff();
	
	/**
	 * Убираем подсветку полей с ошибками
	 */
	$(document).on('focus', '.field-error', function() {
		$(this).removeClass('field-error');
		let Id = $(this).attr('id');
		$('label[for="'+Id+'"]').removeClass('label-error');
	});
	
	/**
	 * Заявка на обратный звонок
	 */
	$(document).on('submit', '.jsContact', function(e) {
		e.preventDefault();
		
		let formData = new FormData($(this)[0]);
		ajax(formData, $(this), true);
	});
	
	/**
	 * Авторизация
	 */
	$(document).on('submit', '.jsAuth', function(e) {
		e.preventDefault();
		
		let email = $(this).find('input[name="email"]').val();
		let password = $(this).find('input[name="password"]').val();
		
		ajax({
			'module': 'auth',
			'ajaxing': 'ajax_auth',
			'email': email,
			'password': password
		}, $(this));
	});
	
	/**
	 * Выход
	 */
	$(document).on('click', '.jsSignOut', function(e) {
		e.preventDefault();
		
		ajax({
			'module': 'auth',
			'ajaxing': 'ajax_sign_out'
		}, false);
	});
	
	
}); /* END of document.ready */