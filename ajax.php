<?php
/**
 * Файл для аджакса в любой модуль
 * $_POST['module'] - сюда присылаем название модуля
 * $_POST['ajaxing'] - сюда присылаем название метода
 * Метод должен иметь префикс ajax_ и возвращать массив с обязательным ключом error
 */
require_once 'init.php';
$tpl = new templates();

// убираем экранирование
foreach ($_POST as $key => $value) {
	if (is_string($value)) {
		$_POST[$key] = stripcslashes($value);
	}
}

// проверяем модуль
if (!MODULE) {
	$add = '';
	if ($_POST['module'] and $_POST['module'] != 'undefined') {
		$add = ' '.$_POST['module'];
	}
	$tpl->json_error('Обращение к несуществующему или неразрешённому модулю'.$add);
}

if (!file_exists(SITE_DIR.'modules/'.MODULE.'.php')) {
	$tpl->json_error('Файл с модулем не найден.');
}

// дополнительная мера безопасности, чтобы нельзя было аджаксить всё подряд
if (substr($_POST['ajaxing'], 0, 5) != 'ajax_') {
	$tpl->json_error('У метода для AJAX должен быть префикс ajax_');
}

require_once(SITE_DIR.'modules/'.MODULE.'.php');
$module = MODULE;
$module = new $module();

if (!method_exists(MODULE, $_POST['ajaxing'])) {
	$tpl->json_error('В модуле <b>'.MODULE.'</b> нет метода <b>'.$_POST['ajaxing'].'</b>');
}
$result_array = $module->{$_POST['ajaxing']}();
echo json_encode($result_array);
