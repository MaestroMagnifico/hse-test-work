<?php
require_once 'init.php';

// Шапка
require_once(SITE_DIR.'templates/header.php');
$tpl = new templates();

// Простая модульная система
if (MODULE) {
	if (!file_exists(SITE_DIR.'modules/'.MODULE.'.php')) {
		$tpl->error('Ошибка', 'Модуль не найден');
	} else {
		require_once(SITE_DIR.'modules/'.MODULE.'.php');
		$module = MODULE;
		$module = new $module();
		$module->view();
	}
} else {
	http_response_code(404);
	$tpl->error('404', 'Страница не найдена');
}

// Подвал
require_once(SITE_DIR.'templates/footer.php');
