<?php
/**
 * Заявка на обратный звонок
 * Class contact
 */
class contact {
	public function view() {
		// этот метод можно использовать как контроллер для вывода
		// шаблон я вынес в отдельный файл
		require(SITE_DIR.'templates/form_contact.php');
	}
	
	/**
	 * Обработка формы через AJAX
	 */
	public function ajax_contact() {
		$db = new db();
		$tpl = new templates();
		$dates = new dates();
		$email = new email();
		
		// проверка необходимых полей
		$data = $db->secure_array($_POST);
		$req = array(
			'fio',
			'phone',
			'date',
			'time'
		);
		$tpl->check_req($data, $req);
		
		// проверка полей на валидность
		if (!preg_match("/^[А-Яа-яЁё -]+$/iu", $data['fio'])) {
			$tpl->json_error('Имя может содержать только буквы, пробелы и "-"', 1, array('fio'));
		}
		
		if (!preg_match("/^[0-9+() -]+$/i", $data['phone'])) {
			$tpl->json_error('Телефон может содержать только цифры, пробел и символы: ()-+', 1, array('phone'));
		}
		
		if ($data['email'] and !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
			$tpl->json_error('E-mail введён в неправильном формате.', 1, array('email'));
		}
		
		$data['date'] .= ' '.$data['time'];
		if ($data['date'] and !$dates->is_valid_date($data['date'])) {
			$tpl->json_error('Неправильный формат даты.', 1, array('date', 'time'));
		}
		
		// сначала отправляем письмо
		$message  = '<p>ФИО: '.$data['fio'].'</p>';
		$message .= '<p>Телефон: '.$data['phone'].'</p>';
		$message .= '<p>E-mail: '.$data['email'].'</p>';
		$message .= '<p>Дата и время звонка: '.$dates->out_date($data['date'], 'full_dig').'</p>';
		try {
			$email->send_email('maestro.magnifico@mail.ru', $message, 'Заявка на обратный звонок');
		} catch (\PHPMailer\PHPMailer\Exception $e) {
			$tpl->error('Ошибка', 'Не удалось отправить письмо: '.$e->getMessage(), false);
		}
		
		// запись
		$db->query("
			INSERT INTO `contact` SET
			`fio`='$data[fio]',
			`phone`='$data[phone]',
			`email`='$data[email]',
			`contact_date`='$data[date]',
			`date`=UTC_TIMESTAMP();
		");
		$db->check_mysqli_error('Ошибка регистрации');
		
		return array('error' => 0, 'redirect' => false, 'message' => 'Ваша заявка отправлена.');
	}
}