<?php
/**
 * Регистрация, авторизация, работа с сессиями
 * Class auth
 */
class auth {
	function view() {
		// этот метод можно использовать как контроллер для вывода
		if (USER) {
			$tpl = new templates();
			$tpl->redirect('/?module=admin');
		} else {
			// шаблон я вынес в отдельный файл
			require (SITE_DIR.'templates/form_login.php');
		}
	}
	
	/**
	 * Проверяет сессию юзера и если она есть, возвращает его таблицу
	 * @param string $pick - какие поля вытащить
	 * @return array|bool - Таблица юзера из БД или FALSE, если сессия не найдена
	 */
	function check_session($pick = '*') {
		$db = new db();
		
		session_start();
		if ($_SESSION['logic-auth']) {
			$auth = $db->secure_str($_SESSION['logic-auth']);
		} else {
			// пытаемся восстановить сессию из куки
			if ($_COOKIE['logic-auth']) {
				$auth = $db->secure_str($_COOKIE['logic-auth']);
			} else {
				// в куках пусто
				session_write_close();
				return false;
			}
		}
		session_write_close();
		
		// проверяем есть ли такая сессия в базе
		$query = $db->query("SELECT $pick FROM `users` WHERE `auth`='$auth' AND `auth` !='' LIMIT 1");
		if (mysqli_num_rows($query) > 0) {
			// возвращаем инфу о юзере
			return mysqli_fetch_array($query, MYSQLI_ASSOC);
		}
		// нет сессии
		return false;
	}
	
	/**
	 * Генерирует уникальный ключ авторизации для пользователя
	 * @throws Exception
	 */
	protected function generate_auth_key() {
		$db = new db();
		
		$auth = false;
		$error = true;
		while($error) {
			$auth = bin2hex(random_bytes(32));
			$auth = substr($auth, 0, 32);
			
			$query = $db->query("SELECT 'id' FROM `users` WHERE `auth`='$auth'");
			$row = mysqli_fetch_array($query, MYSQLI_ASSOC);
			if (!$row['id']) {
				$error = false;
			}
		}
		return $auth;
	}
	
	/**
	 * Авторизация через AJAX (простая защита формы от brute force)
	 * @return array
	 * @throws Exception
	 */
	function ajax_auth() {
		$db = new db();
		$tpl = new templates();
		
		$email = trim($_REQUEST['email']);
		$password = $db->escape($_REQUEST['password']);
		
		if (!$email) {
			$tpl->json_error('Вы не ввели E-mail.', array('email'));
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$tpl->json_error('Введён E-mail в неправильном формате.', array('email'));
		}
		
		if (!$password) {
			$tpl->json_error('Вы не ввели пароль.', array('password'));
		}
		
		$email = $db->secure_str($email);
		$password = md5($password);
		
		$query = $db->query("SELECT `id` FROM `users` WHERE `email`='$email' AND `email` != '' AND `password`='$password' AND `password` != ''");
		$db->check_mysqli_error('Ошибка при авторизации');
		$row = mysqli_fetch_array($query, MYSQLI_ASSOC);
		if (!$row['id']) {
			$tpl->json_error('Такая комбинация логина и пароля не найдена.');
		}
		$user_id = $row['id'];
		
		// генерируем уникальный ключ авторизации
		$auth = $this->generate_auth_key();
		
		// прописываем пользователю ключ авторизации
		$db->query("UPDATE `users` SET `auth`='$auth' WHERE `id`=$user_id");
		$db->check_mysqli_error('Ошибка авторизации');
		
		// пишем ключ в сессию и куки
		session_start();
		$_SESSION['logic-auth'] = $auth;
		session_write_close();
		setcookie('logic-auth', $auth, time() + 86400, '/',"", false, true);
		
		return array('error' => 0, 'redirect' => '?module=admin');
	}
	
	/**
	 * Сброс авторизации
	 * @return array
	 */
	function ajax_sign_out() {
		session_start();
		unset($_SESSION['logic-auth']);
		session_write_close();
		setcookie('logic-auth', null, -1, '/');
		return array('error' => 0, 'redirect' => '/');
	}
}