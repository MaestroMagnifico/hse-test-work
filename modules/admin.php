<?php
/**
 * Админский раздел с выводом заявок
 * Class admin
 */
class admin {
	function view() {
		$tpl = new templates();
		if (!USER) {
			// нет авторизации
			$tpl->error('Ошибка', 'Вы не авторизованы');
		}
		
		$db = new db();
		$modify = new modify();
		$dates = new dates();
		
		$rows = array();
		$query = $db->query("SELECT * FROM `contact` ORDER BY `contact_date`");
		$db->check_mysqli_error();
		while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
			$row['fio'] = $modify->form_text($row['fio']);
			$row['phone'] = $modify->form_text($row['phone']);
			$row['email'] = $modify->form_text($row['email']);
			$row['contact_date'] = $dates->out_date($row['contact_date'], 'full_dig');
			$rows[] = $row;
		}
		
		require (SITE_DIR.'templates/admin.php');
	}
}